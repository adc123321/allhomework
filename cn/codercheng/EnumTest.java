package cn.codercheng;

/**
 * @ClassName EnumTest
 * @Description:
 * @Author CoderCheng
 * @Date 2020-08-22 17:32
 * @Version V1.0
 **/
public enum  EnumTest {

    AA,

    BB
}
